<?php
/**
 * @package     Ads Elite
 * @subpackage  mod_ads_elite
 * @copyright   Copyright (C) 2013 Elite Developers All rights reserved.
 * @license   	GNU/GPL v3 http://www.gnu.org/licenses/gpl.html
 */

defined('_JEXEC') or die( 'Restricted access' );
?>
<div class="ads<?php echo $moduleclass_sfx ?>">
	<?php echo $adscode; ?>
</div>